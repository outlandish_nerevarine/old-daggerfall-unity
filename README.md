This repository is outdated and old. It's been superseded by DFInterkarma's official "Daggerfall Unity," a project of the same name. The changes that were required in my local copy of DFTFU are no longer necessary.

----

# Daggerfall Tools for Unity

Daggerfall Tools for Unity is a code asset which acts as a bridge between Daggerfall’s binary data files and Unity3D.
It consists of a mature API for opening and converting almost all of Daggerfall’s content files – plus a set of editor
windows, example classes, and prefabs for quickly converting Daggerfall’s environments into fully functional Unity scenes.

For more information: http://www.dfworkshop.net/?page_id=1224